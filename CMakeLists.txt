## CMakeLists.txt for WtPyext package to be built within ATLAS
## Analysis Relase 21.2.X series.
## Author: Doug Davis <ddavis@cern.ch>

# Declare the name of this package:
atlas_subdir(WtPyext None)

atlas_depends_on_subdirs(PUBLIC TopLoop)

find_package(ZLIB REQUIRED)
find_package(ROOT REQUIRED COMPONENTS Physics Core Tree Hist RIO MathCore TreePlayer TMVA)

atlas_add_library(WtPyext
  WtPyext/*.h Root/*.cpp
  PUBLIC_HEADERS WtPyext
  SHARED
  LINK_LIBRARIES ${ZLIB_LIBRARIES})

target_compile_features(WtPyext PUBLIC cxx_std_14)

atlas_add_executable(augment-tree-with-npy
  util/augment-tree-with-npy.cpp
  LINK_LIBRARIES ${ROOT_LIBRARIES} WtPyext ${ZLIB_LIBRARIES})

target_include_directories(augment-tree-with-npy PRIVATE ${ROOT_INCLUDE_DIRS})

# Install python modules
atlas_install_python_modules(python/*)

atlas_install_scripts(scripts/*)
